﻿using Blog1._0.Data;
using Blog1._0.Service;
using Cache.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Cache
{
    public class MenuCache : Repository
    {
        private Sys_MenuService sys_MenuService = new Sys_MenuService();

        /// <summary>
        /// 一级菜单
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuInfo> GetListMenuInfo()
        {
            var cacheList = CacheFactory.Cache().GetCache<IEnumerable<MenuInfo>>(sys_MenuService.cacheKey + "0");
            if (cacheList == null)
            {
                var data = sys_MenuService.GetMenuList();
                CacheFactory.Cache().WriteCache(data, sys_MenuService.cacheKey + "0");
                return data;
            }
            else
            {
                return cacheList;
            }
        }

        /// <summary>
        /// 分类标签
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuInfo> GetListLabelInfo()
        {
            var cacheList = CacheFactory.Cache().GetCache<IEnumerable<MenuInfo>>(sys_MenuService.cacheLabelKey + "0");
            if (cacheList == null)
            {
                var data = sys_MenuService.GetListLabelInfo();
                CacheFactory.Cache().WriteCache(data, sys_MenuService.cacheLabelKey + "0");
                return data;
            }
            else
            {
                return cacheList;
            }
        }
    }
}
