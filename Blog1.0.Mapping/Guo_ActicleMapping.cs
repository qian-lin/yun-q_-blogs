﻿using Blog1._0.Model;
using System.Data.Entity.ModelConfiguration;

namespace Blog1._0.Mapping
{
    public class Guo_ActicleMapping : EntityTypeConfiguration<Guo_Acticle>
    {
        public Guo_ActicleMapping()
        {
            this.ToTable("Guo_Acticle");
            this.HasKey(t => t.ArticleID);
        }
    }
}
