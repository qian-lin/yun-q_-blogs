﻿using Blog1._0.Model;
using System.Data.Entity.ModelConfiguration;

namespace Blog1._0.Mapping
{
    public class Guo_ResourcesMapping : EntityTypeConfiguration<Guo_Resources>
    {
        public Guo_ResourcesMapping()
        {
            this.ToTable("Guo_Resources");
            this.HasKey(t => t.Id);
        }
    }
}
