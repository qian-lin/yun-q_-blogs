﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapping
{
    public class Guo_Sys_MenuMapping : EntityTypeConfiguration<Guo_Sys_Menu>
    {
        public Guo_Sys_MenuMapping()
        {
            this.ToTable("Guo_Sys_Menu");
            this.HasKey(t => t.MenuID);
        }
    }
}
