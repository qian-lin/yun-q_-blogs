﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    public partial class Guo_AdminMenu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 菜单地址
        /// </summary>
        public string MenuUrl { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string MenuIcon { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public Nullable<int> OrderNo { get; set; }
        /// <summary>
        /// 父级菜单
        /// </summary>
        public Nullable<int> ParentId { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Nullable<bool> Status { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<System.DateTime> CreateOn { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public Nullable<System.DateTime> UpdateOn { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        public Nullable<int> CreateBy { get; set; }
        /// <summary>
        /// 编辑者
        /// </summary>
        public Nullable<int> UpdateBy { get; set; }       
    }
}
