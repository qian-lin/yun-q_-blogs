﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    /// <summary>
    /// 按钮权限
    /// </summary>
    public partial class Guo_Menu_Action
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        /// <summary>
        /// 菜单ID
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// 操作ID
        /// </summary>
        public int ActionId { get; set; }
    }
}
