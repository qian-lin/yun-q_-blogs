﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    public partial class Guo_Role
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]//代表ID自增
        public int Id { get; set; }
        /// <summary>
        /// 角色编码
        /// </summary>
        public string LoginID { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 角色描述
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Nullable<bool> Status { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<System.DateTime> CreateOn { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public Nullable<System.DateTime> UpdateOn { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        public Nullable<int> CreateBy { get; set; }
        /// <summary>
        /// 修改者
        /// </summary>
        public Nullable<int> UpdateBy { get; set; }

    }
}
