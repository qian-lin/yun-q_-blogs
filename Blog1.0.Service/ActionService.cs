﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 按钮权限处理 类
    /// </summary>
    public class ActionService : Repository
    {
        /// <summary>
        /// 根据角色ID获取按钮权限
        /// </summary>
        /// <param name="menuId"></param>
        /// <param name="roleId"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public IEnumerable<ActionModel> GetActionListByMenuIdRoleId(int menuId, int? roleId, PositionEnum position)
        {
            var db = this.Base();
            var pos = Convert.ToInt32(position);
            var menu_role = db.IQueryable<Guo_Admin_Role_Menu>();
            var menu = db.IQueryable<Guo_Action>();
            var query = from m in menu_role
                        join t in menu on m.ActionId equals t.ID
                        where m.MenuId == menuId && m.RoleId == roleId
                        && t.Position == pos
                        select new ActionModel()
                        {
                            ActionCode = t.ActionCode,
                            ActionName = t.ActionName,
                            Position = t.Position,
                            Icon = t.Icon,
                            Method = t.Method,
                            Remark = t.Remark,
                            OrderBy = t.OrderBy,
                            ClassName = t.ClassName
                        };
            return query.ToList();
        }

        /// <summary>
        /// 返回菜单权限按钮HTML字符串
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public string GetActionListHtmlByRoleId(int roleId, int menuId)
        {
            var db = this.Base();
            IEnumerable<ActionModel> selectList = null;
            var allList = GetActionListByRoleId(roleId, menuId, out selectList);
            StringBuilder sb = new StringBuilder();
            if (allList != null && allList.Count() > 0)
            {
                foreach (var a in allList)
                {
                    var checkedStr = selectList.FirstOrDefault(x => x.Id == a.Id) == null ? "" : "checked= ''";
                    sb.AppendFormat("<input name='cbx_{0}' lay-skin='primary' value='{1}' title='{2}' type='checkbox' {3}>", menuId, a.Id, a.ActionName, checkedStr);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// 根据角色ID、菜单ID查询显示 对应权限按钮字符串
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="menuId"></param>
        /// <param name="selectList"></param>
        /// <returns></returns>
        public IEnumerable<ActionModel> GetActionListByRoleId(int roleId, int menuId, out IEnumerable<ActionModel> selectList)
        {
            var db = this.Base();
            var action = db.IQueryable<Guo_Action>();
            var role = db.IQueryable<Guo_Admin_Role_Menu>(t => t.RoleId == roleId && t.MenuId == menuId);
            var menu_action = db.IQueryable<Guo_Menu_Action>(t => t.MenuId == menuId);

            var query = from q in action
                        join w in role on
                        q.ID equals w.ActionId
                        select new ActionModel()
                        {
                            Id = q.ID,
                            ActionName = q.ActionName
                        };
            selectList = query.ToList();

            var query1 = from q in action
                         join t in menu_action on
                         q.ID equals t.ActionId
                         orderby q.OrderBy
                         select new ActionModel()
                         {
                             Id = q.ID,
                             ActionName = q.ActionName
                         };
            return query1.ToList();
        }

        /// <summary>
        /// 查询是否存在对应的权限记录
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public IEnumerable<MenuRoleActionModel> GetListByRoleIdMenuId(int roleId, int menuId)
        {
            var db = this.Base();
            var roled = db.IQueryable<Guo_Admin_Role_Menu>(t => t.RoleId == roleId && t.MenuId == menuId);
            var data = from i in roled
                       select new MenuRoleActionModel
                       {
                           MenuId = i.MenuId,
                           ActionId = i.ActionId,
                           RoleId = i.RoleId
                       };

            return data;
        }
    }
}
