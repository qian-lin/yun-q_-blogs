﻿using Blog1._0.Data;
using Blog1._0.Model;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【后台管理首页菜单加载】逻辑层
    /// </summary>
    public class AdminService : Repository
    {
        private void SetTree(TreeMenu _tree, string menuUrl, bool isIndex)
        {
            if (isIndex)
            {
                _tree.href = menuUrl;
            }
        }
        /// <summary>
        /// 获取菜单列表
        /// </summary>
        /// <param name="isIndex"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public dynamic GetMenusList(bool isIndex, int? roleId)
        {
            var db = this.Base();
            //根据角色ID获得菜单列表
            IEnumerable<MenuModel> allMenus = GetMenuListByRoleId(roleId);
            List<TreeMenu> treeList = new List<TreeMenu>();
            var rootMenus = allMenus.Where(x => x.ParentId == 0).OrderBy(x => x.OrderNo);
            int index = 0;
            foreach (var r in rootMenus)
            {
                var _tree = new TreeMenu { id = r.Id, name = r.MenuName, title = r.MenuName, icon = r.MenuIcon };
                if (isIndex == true && index == 0) //首页菜单默认第一项展开
                {
                    _tree.spread = true;
                }
                index++;
                //根据一级菜单加载子菜单列表
                GetMenusByRootMenuId(treeList, allMenus, _tree, r.Id, isIndex);
                treeList.Add(_tree);
            }
            var result = treeList;
            return treeList;

        }

        /// <summary>
        /// 根据角色ID获得菜单列表
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        private IEnumerable<MenuModel> GetMenuListByRoleId(int? roleId)
        {
            var db = this.Base();
            var menu_role = db.IQueryable<Guo_Admin_Role_Menu>();
            var menu = db.IQueryable<Guo_AdminMenu>();
            var query = from m in menu_role
                        join t in menu on m.MenuId equals t.ID
                        where m.RoleId == roleId
                        where t.Status == true
                        group t by new
                        {
                            t.ID,
                            t.MenuName,
                            t.MenuIcon,
                            t.OrderNo,
                            t.ParentId,
                            t.MenuUrl
                        } into g
                        select new MenuModel()
                        {
                            Id = g.Key.ID,
                            MenuName = g.Key.MenuName,
                            MenuIcon = g.Key.MenuIcon,
                            OrderNo = g.Key.OrderNo,
                            ParentId = g.Key.ParentId,
                            MenuUrl = g.Key.MenuUrl
                        };
            return query;
        }

        /// <summary>
        /// 根据一级菜单加载子菜单列表
        /// </summary>
        /// <param name="treeList"></param>
        /// <param name="allMenus"></param>
        /// <param name="tree"></param>
        /// <param name="menuId"></param>
        /// <param name="isIndex"></param>
        private void GetMenusByRootMenuId(List<TreeMenu> treeList, IEnumerable<MenuModel> allMenus, TreeMenu tree, int menuId, bool isIndex)
        {
            var childMenus = allMenus.Where(x => x.ParentId == menuId).OrderBy(x => x.OrderNo);
            if (childMenus != null && childMenus.Count() > 0)
            {
                List<TreeMenu> _children = new List<TreeMenu>();
                foreach (var m in childMenus)
                {
                    var _tree = new TreeMenu { id = m.Id, name = m.MenuName, title = m.MenuName, icon = m.MenuIcon };
                    SetTree(_tree, m.MenuUrl, isIndex);
                    _children.Add(_tree);
                    tree.children = _children;
                    GetMenusByRootMenuId(treeList, allMenus, _tree, m.Id, isIndex);
                }
            }
        }


    }
}
