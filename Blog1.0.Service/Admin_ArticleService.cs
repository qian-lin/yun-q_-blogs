﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【文章管理】逻辑层
    /// </summary>
    public class Admin_ArticleService : Repository
    {
        /// <summary>
        /// 获取 标签分类
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuInfo> GetTypeList()
        {
            var db = this.Base();
            var type = db.IQueryable<Guo_Catalog>();
            var article = db.IQueryable<Guo_Acticle>();
            var query = from u in type                       
                        orderby new { u.CRT_Time, u.OrderNo } descending
                        select new MenuInfo
                        {
                            id = u.CatalogID,
                            name = u.CatalogName,
                            isParent = u.IsActive,
                            pId = article.Where(t => t.CatalogID == u.CatalogID).Count()
                        };
            var Lists = query.ToList();
            for (int i = 0; i < Lists.Count(); i++)
            {
                Lists[i].file = Des.DesEncrypt("guo", Lists[i].id.ToString());
            }
            return Lists;
        }

        /// <summary>
        /// 获取 文章类型
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuInfo> GetArticleTypeList()
        {
            var db = this.Base();
            var type = db.IQueryable<Guo_Type>();
            var query = from u in type
                        select new MenuInfo
                        {
                            id = u.TypeID,
                            name = u.TypeName
                        };
            return query;
        }

        /// <summary>
        /// 线性查询 文章列表 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(Guo_Acticle filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var article = db.IQueryable<Guo_Acticle>();
            var type = db.IQueryable<Guo_Type>();
            var catalog = db.IQueryable<Guo_Catalog>();
            var query = from i in article
                        join k in type
                        on i.TypeID equals k.TypeID
                        join o in catalog
                        on i.CatalogID equals o.CatalogID
                        orderby i.CRT_Time descending
                        select new
                        {
                            Id = i.ArticleID,
                            Title = i.ArticleTitle,
                            ImgUrl = i.ImgUrl,
                            TypeID = i.TypeID,
                            CatalogID = i.CatalogID,
                            TypeName = k.TypeName,
                            CatalogName = o.CatalogName,
                            IsTop = i.IsTop,
                            ViewTimes = i.ViewTimes,
                            Replies = i.Replies,
                            IsActive = i.IsActive,
                            CRT_Time = i.CRT_Time
                        };
            //文章标题
            if (!string.IsNullOrEmpty(filter.ArticleTitle))
            {
                string articleTitle = filter.ArticleTitle.ToString();
                query = query.Where(t => t.Title.Contains(articleTitle));
            }
            //文章属性
            if (filter.TypeID != 0)
            {
                int typeID = Convert.ToInt16(filter.TypeID);
                query = query.Where(t => t.TypeID == typeID);
            }
            //文章分类
            if (filter.CatalogID != 0)
            {
                int catalogID = Convert.ToInt16(filter.CatalogID);
                query = query.Where(t => t.CatalogID == catalogID);
            }
            //文章状态
            if (filter.IsActive != null)
            {
                bool isActive = Convert.ToBoolean(filter.IsActive);
                query = query.Where(t => t.IsActive == isActive);
            }

            var data = query.Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);//分页
            return new LayuiResult { code = 0, count = query.Count(), data = data.ToList() };
        }

        /// <summary>
        /// 编辑文章-数据加载到页面
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public IEnumerable<ArticleModel> GetDetail(int ID)
        {
            var db = this.Base();
            var article = db.IQueryable<Guo_Acticle>();
            var article_catlog = db.IQueryable<Guo_Catalog>();
            var type = db.IQueryable<Guo_Type>();
            var query = from i in article
                        join t in article_catlog
                        on i.CatalogID equals t.CatalogID
                        join o in type
                        on i.TypeID equals o.TypeID
                        where i.ArticleID == ID
                        select new ArticleModel()
                        {
                            ArticleID = i.ArticleID,
                            ArticleTitle = i.ArticleTitle,
                            Abstract = i.Abstract,
                            ImgUrl = i.ImgUrl,
                            Content = i.Content,
                            TypeID = i.TypeID,
                            TypeName = o.TypeName,
                            CatalogID = i.CatalogID,
                            CatalogName = t.CatalogName,
                            IsTop = i.IsTop,
                            ViewTimes = i.ViewTimes,
                            IsActive = i.IsActive,
                            CRT_Time = i.CRT_Time,
                            Up_Time = i.Up_Time,
                            Wyyun = i.Wyyun

                        };
            return query.ToList();
        }

        /// <summary>
        /// 修改 文章方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(ArticleModel model)
        {
            var db = this.Base();
            var art = db.IQueryable<Guo_Acticle>(t => t.ArticleID == model.ArticleID).FirstOrDefault();
            art.TypeID = model.TypeID;
            art.CatalogID = model.CatalogID;
            art.ArticleTitle = model.ArticleTitle;
            art.ImgUrl = model.ImgUrl;
            art.Content = model.Content;
            art.Abstract = model.Abstract;
            art.Up_Time = DateTime.Now;
            art.IsTop = model.IsTop;
            art.IsActive = model.IsActive;
            art.Wyyun = model.Wyyun;

            db.Update(art);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 删除指定文章
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_Acticle>(t => t.ArticleID == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_Acticle>(t => t.ArticleID == entity.ArticleID);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                // db.Rollback();
                throw new Exception("删除失败！");
            }
        }

        /// <summary>
        /// 新增文章 方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateModel(ArticleModel model)
        {
            var db = this.Base();
            Guo_Acticle article = new Guo_Acticle();
            db.SaveChanges();

            article.TypeID = model.TypeID;
            article.CatalogID = model.CatalogID;
            article.ArticleTitle = model.ArticleTitle;
            article.ImgUrl = model.ImgUrl;
            article.Content = model.Content;
            article.Abstract = model.Abstract;
            article.UserID = Current.GetEntity().OperatorId;
            article.CRT_Time = model.CRT_Time;
            article.Up_Time = model.Up_Time;
            article.IsTop = model.IsTop;
            article.IsActive = model.IsActive;
            article.Wyyun = model.Wyyun;
            db.Insert(article);
            //提交失败，则检查实体类是否加上自增ID标识
            return db.Commit() > 0 ? true : false;

        }

        /// <summary>
        /// 返回今日发文数量
        /// </summary>
        /// <returns></returns>
        public int GetArticleNum()
        {
            var db = this.Base();
            var article = db.IQueryable<Guo_Acticle>();
            var query = from i in article
                        where i.IsActive == true
                        select i;
            var timeNow = DateTime.Now.ToString("yyyy-MM-dd");
            DateTime time1 = Convert.ToDateTime(timeNow + " 0:00:00");  //数字前记得加空格
            DateTime time2 = Convert.ToDateTime(timeNow + " 23:59:59");

            query = query.Where(t => t.CRT_Time > time1 && t.CRT_Time < time2);
            return query.Count();
        }
    }
}
