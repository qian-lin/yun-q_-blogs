﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【日记管理】逻辑层
    /// </summary>
    public class DiarysService : Repository
    {
        /// <summary>
        /// 日记数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(Guo_Diays filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var diary = db.IQueryable<Guo_Diays>();
            var query = (from i in diary
                         orderby i.CreateOn descending
                         select i).Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);

            return new LayuiResult { code = 0, count = diary.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 日记新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateModel(DiarysModel model)
        {
            var db = this.Base();
            Guo_Diays diay = new Guo_Diays();
            db.SaveChanges();
            diay.Content = model.Content;
            diay.CreateOn = model.CreateOn;

            db.Insert<Guo_Diays>(diay);
            //提交失败，则检查实体类是否加上自增ID标识
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 查看日记
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<DiarysModel> ReadModel(int Id)
        {
            var db = this.Base();
            var data = db.IQueryable<Guo_Diays>().Where(t => t.Id == Id);
            var query = from i in data
                        select new DiarysModel()
                        {
                            Id = i.Id,
                            Content = i.Content,
                            CreateOn = i.CreateOn
                        };

            return query.ToList();
        }

        /// <summary>
        /// 修改日记
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(DiarysModel model)
        {
            var db = this.Base();
            var res = db.IQueryable<Guo_Diays>(t => t.Id == model.Id).FirstOrDefault();
            res.Content = model.Content;

            db.Update(res);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_Diays>(t => t.Id == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_Diays>(t => t.Id == entity.Id);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                throw new Exception("删除失败！");
            }
        }
    }
}
