﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Data.SqlClient;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【登录】逻辑层
    /// </summary>
    public class LoginService : Repository
    {
        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="userCode"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Result UserLogin(string userCode, string password)
        {
            try
            {
                var db = this.Base();
                var user = db.FindEntity<Guo_User>(t => t.LoginID == userCode);
                if (user != null)
                {
                    //SqlParameter[] sql = { new SqlParameter("@strSource", password), new SqlParameter("@EncPara", user.ID) };
                    //var pwd = db.ExecuteByFun<string>("BRPEncrypt", sql);  //调用数据库函数对输入密码加密
                    if (password == user.PassWord)
                    {
                        var current = new Operator();
                        current.OperatorId = user.ID;
                        current.RoleId = user.RoleId;
                        current.UserName = user.UserName;
                        current.LoginID = user.LoginID;
                        current.HeadShot = user.HeadShot;
                        current.IPAddressName = Net.Host;
                        current.IPAddress = Net.Ip;
                        current.LoginTime = DateTime.Now.ToString();
                        return new Result { type = true, msg = "登录成功", data = current };
                    }
                    else
                    {
                        return new Result { type = false, msg = "密码错误" };
                    }
                }
                else
                {
                    return new Result { type = false, msg = "账号名不存在" };
                }
            }
            catch (Exception)
            {
                throw new Exception("操作异常，请联系管理员！！！");
            }

        }
    }
}
