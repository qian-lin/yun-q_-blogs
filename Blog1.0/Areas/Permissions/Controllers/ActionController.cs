﻿using Blog1._0.Model;
using Blog1._0.Service;
using System;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Permissions.Controllers
{
    /// <summary>
    /// 【操作管理】控制器
    /// </summary>
    public class ActionController : MvcControllerBase
    {
        Admin_ActionService service = new Admin_ActionService();
        /// <summary>
        /// 操作管理视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            } 
            #endregion
            return View();
        }

        /// <summary>
        /// 新增 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            ViewData["Position"] = EnumExt.GetSelectList(typeof(PositionEnum));
            return View();
        }

        /// <summary>
        /// 详情 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            var model = service.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 编辑 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id)
        {
            ViewData["Position"] = EnumExt.GetSelectList(typeof(PositionEnum));
            var model = service.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult List(Guo_Action filter, PageInfo pageInfo)
        {
            var result = service.GetListByFilter(filter, pageInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 新增方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(ActionModel model)
        {
            model.CreateOn = DateTime.Now;
            model.CreateBy = Current.GetEntity().OperatorId;
            var result = service.CreateModel(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 保存修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(ActionModel model)
        {
            model.UpdateOn = DateTime.Now;
            model.UpdateBy = Current.GetEntity().OperatorId;
            var result = service.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除按钮
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            //删除权限时,同时删除菜单权限,菜单角色权限记录
            var result = service.DeleteModel(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }


    }
}