﻿using Blog1._0.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Permissions.Controllers
{
    /// <summary>
    /// 【角色管理】控制器
    /// </summary>
    public class RoleController : MvcControllerBase
    {
        Admin_RoleService service = new Admin_RoleService();
        /// <summary>
        /// 角色管理 首页视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            } 
            #endregion
            return View();
        }

        /// <summary>
        /// 新增 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 详情视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            var model = service.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 修改 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id)
        {
            var model = service.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 角色权限分配 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Assign(int Id)
        {
            ViewBag.RoleId = Id;
            return View();
        }

        /// <summary>
        /// 新增 方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(RoleModel model)
        {
            model.CreateOn = DateTime.Now;
            model.CreateBy = Current.GetEntity().OperatorId;
            var result = service.CreateModel(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="pageInfo"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult List(PageInfo pageInfo, RoleModel filter)
        {
            var result = service.GetListByFilter(filter, pageInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(RoleModel model)
        {
            model.UpdateOn = DateTime.Now;
            model.UpdateBy = Current.GetEntity().OperatorId;
            var result = service.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            //删除角色时,同时删除菜单角色权限记录
            var result = service.DeleteModel(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 角色权限分配
        /// </summary>
        /// <param name="pageInfo"></param>
        /// <param name="filter"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult MenuActionLists(PageInfo pageInfo, MenuModel filter, int roleId)
        {
            var list = service.GetAvailableMenuList(roleId);
            var result = new { code = 0, count = list.Count(), data = list };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存角色权限更改
        /// </summary>
        /// <param name="list"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertBatch(IEnumerable<MenuRoleActionModel> list, int roleId)
        {
            var result = service.GetInsertBash(list, roleId) ? SuccessTip() : ErrorTip();
            return Json(result);
        }
    }
}