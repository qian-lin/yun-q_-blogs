﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【自定义错误页 控制器】(弃用)
    /// 因为把错误页写进控制器的话，服务器错误以及数据库错误，在浏览器上看不到，所以直接用静态html即可
    /// </summary>
    public class ErrorController : Controller
    {
        // 404 Error
        public ActionResult ErrorPath404()
        {
            return View();
        }

        // 403 Error
        public ActionResult ErrorPath403()
        {
            return View();
        }

        // 500 Error
        public ActionResult ErrorPath500()
        {
            return View();
        }
    }
}