﻿using Blog1._0.Service;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【资源】控制器
    /// </summary>
    public class ResourcesController : Controller
    {
        ResourcesService service = new ResourcesService();
        /// <summary>
        /// 首页视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? Id=0)
        {
            ViewBag.ClassList = service.GetTypeList().ToList();//获取文章or标签分类
            //每页显示数目
            int PageSize = 5;
            ViewBag.PageSize = PageSize;
            //总条数
            int Count = service.GetDemoNum();
            ViewBag.Count = Count;

            //总页数
            int PageCount = (Count + PageSize - 1) / PageSize;
            ViewBag.PageCount = PageCount;

            ViewBag.ClassId = Id;
            return View();
        }

        /// <summary>
        /// 详情视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            WebSiteInfo siteInfo = new WebSiteInfo();
            //获取节点key值
            ViewBag.Site = siteInfo.GetWebSiteInfo();
            return View();
        }

        /// <summary>
        /// 资源分页查询
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpPost]
        public ContentResult LoadResourceByClass(int classId, int page, int pagesize)
        {
            string result = service.LoadResourceByClass(classId, page, pagesize);
            return Content(result);
        }
    }
}