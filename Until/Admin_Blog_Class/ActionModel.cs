﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    public class ActionModel
    {
        public int Id { get; set; }
        /// <summary>
        /// 操作编码
        /// </summary>
        public string ActionCode { get; set; }
        /// <summary>
        /// 操作名称
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// 显示位置
        /// </summary>
        public int? Position { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 方法名称
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public int? OrderBy { get; set; }
        /// <summary>
        /// 样式名称
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateOn { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public int CreateBy { get; set; }
        /// <summary>
        /// 修改者
        /// </summary>
        public int UpdateBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateOn { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public bool? Status { get; set; }
    }
}
