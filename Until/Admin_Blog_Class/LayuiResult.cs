﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    /// <summary>
    /// Layui 表格格式返回类
    /// </summary>
    public class LayuiResult
    {
        /// <summary>
        /// 操作结果类型
        /// </summary>
        public int code { get; set; }
        /// <summary>
        /// 返回总条数
        /// </summary>
        public int count { get; set; }
        /// <summary>
        /// 具体数据
        /// </summary>
        public object data { get; set; }
    }
}
