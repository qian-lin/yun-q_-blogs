﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    /// <summary>
    /// 文件上传 返回类
    /// </summary>
    public class uploadFile
    {
        /// <summary>
        /// 返回请求状态码
        /// </summary>
        public int code { get; set; }
        /// <summary>
        /// 提示信息
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// "data": {
        ///    "src": "图片路径"
        ///    ,"title": "图片名称" //可选
        ///  }
        /// </summary>
        public dynamic data { get; set; }
    }
}
