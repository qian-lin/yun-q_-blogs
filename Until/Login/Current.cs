﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Until;

namespace Until
{
    public static class Current
    {
        public static bool isAllowLogin = true;         //是否允许登录

        /// <summary>
        /// 获取当前用户
        /// </summary>
        /// <returns></returns>
        public static Operator GetEntity()
        {
            HttpContext context = HttpContext.Current;           
            var user = context.Session["Operator"] as Operator;
            return user;
        }       
       
    }
}
