﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    /// <summary>
    /// Login过滤器枚举类
    /// </summary>
    public enum LoginMode
    {
        /// <summary>执行</summary>
        Enforce,
        /// <summary>忽略</summary>
        Ignore
    }
}
