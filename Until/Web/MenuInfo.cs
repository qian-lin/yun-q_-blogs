﻿namespace Until
{
    /// <summary>
    /// 菜单实体类
    /// </summary>
    public class MenuInfo
    {
        public int id { get; set; }
        public int pId { get; set; }
        public string name { get; set; }
        public bool? open { get; set; }
        public string file { get; set; }
        public short? left { get; set; }
        public short? right { get; set; }
        public short rel { get; set; }
        public bool? isParent { get; set; }
        public string icon { get; set; }
    }
}
