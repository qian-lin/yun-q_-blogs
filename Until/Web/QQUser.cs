﻿using System;

namespace Until
{
    /// <summary>
    /// qq 绑定实体类
    /// </summary>
    public class QQUser
    {
        public int Id { get; set; }
        /// <summary>
        /// qq昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int Gender { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadShot { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Nullable<bool> Status { get; set; }
    }
}
