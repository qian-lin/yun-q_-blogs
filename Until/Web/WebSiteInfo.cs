﻿namespace Until
{
    public class WebSiteInfo
    {
        /// <summary>
        /// 返回webconfig自定义key值
        /// </summary>
        /// <returns></returns>
        public WebSiteModel GetWebSiteInfo()
        {
            return new WebSiteModel
            {
                MaxCommentNum = Configs.GetValue("MaxCommentNum"),
                MaxFeedbackNum = Configs.GetValue("MaxFeedbackNum"),
                OpenComment = Configs.GetValue("OpenComment"),
                OpenFeedback = Configs.GetValue("OpenFeedback"),
                SiteName = Configs.GetValue("SiteName"),
                SiteTitle = Configs.GetValue("SiteTitle"),
                SiteDomain = Configs.GetValue("SiteDomain"),
                QQ = Configs.GetValue("QQ"),
                Mail = Configs.GetValue("Mail"),
                Address = Configs.GetValue("Address"),
                Gitee = Configs.GetValue("Gitee"),
                CacheTime = Configs.GetValue("CacheTime"),
                MaxFileUpload = Configs.GetValue("MaxFileUpload"),
                UploadFileType = Configs.GetValue("UploadFileType"),
                HomeTitle = Configs.GetValue("HomeTitle"),  
                MetaKey = Configs.GetValue("MetaKey"),
                MetaDescribe = Configs.GetValue("MetaDescribe"),
                CopyRight = Configs.GetValue("CopyRight"),
                Frontface = Configs.GetValue("Frontface"),
                Bokeface = Configs.GetValue("Bokeface"),
                AboutFont = Configs.GetValue("AboutFont")
            };
        }
    }
}
